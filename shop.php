<?php include("header.php"); ?>
		<section id="body_area">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<?php include("left-sidebar.php"); ?>
					</div>
					<div class="col-md-9">
						<div class="product_shop_item"> 
					<?php
						$flag = false;
						$sopid   = (int)$_GET['sub_cate'];							
						if(isset($sopid)):						
						$perpage = 9;
						$total	= $data->getCountDatabySubCate($sopid);
						if($total==0):
							echo("<center><h3>Sorry, data not found</h3></center>");											$flag = true;
						endif;
						$page=(isset($_GET['page'])&& $_GET['page']>0) ? (int)$_GET['page'] : 1;
						$prolimit = ($page > 1) ? ($page * $perpage)-$perpage : 0;
						$prev = $page-1;
						$next = $page+1;
						$prevUrl =	"?sub_cate=$sopid&page=$prev";
						$nextUrl =	"?sub_cate=$sopid&page=$next";
						$listUrl = 	"?sub_cate=$sopid&page=";
						$pages = ceil($total/$perpage);						
							$getShop = $data->getLimtData('shopkeeper',"sub_category_id =",$sopid,$prolimit,$perpage);	
							if($getShop):				
							foreach($getShop as $shoplist):
					?>
						<div class="col-md-4"> 
							<div class="single_product_shops"> 
								<center>
									<img src="site_img/header_image/<?php echo $shoplist['header_img']; ?>" class="img-responsive" alt="" />
									<a href="single-product.php?proid=<?php echo $shoplist['id']; ?>"><h4><?php echo $shoplist['shop_name']; ?></h4></a>
										<h5>
							<?php 
								
								//$address = explode(" ", trim($shoplist['shop_description']));
								//$first_address = implode(" ", array_splice($address, 0, 6));
								//echo $first_address."..."; 
							
							?>
										</h5>
										<h6><b>মোবাইল </b> : <?php echo $shoplist['phone_number']; ?></h6>
									<!--	<p><b>অবস্থান</b> : <?php //echo $data->stringToWord($shoplist['address'],5)."..."; ?></p>-->
									<a href="single-product.php?proid=<?php echo $shoplist['id']; ?>"><button type="button" class="btn btn-primary btn-block">আরো পড়ুন ..</button></a>		
								</center>
							</div>							
						</div>
					<?php
							endforeach;
							else: 
								if(!$flag):
									echo("<center><h3>Sorry, Something Wrong</h3></center>");
								endif;					
							endif;					
						endif;					
					?>	
					</div>
					</div>	
				</div>
				<nav>
					  <ul class="pager">	
					   <?php if($page!=1){ ?>
					    <li><a href="<?php echo $_SERVER['PHP_SELF'].$prevUrl; ?>">Previous</a></li>
					    
					    <?php } ?>
					  
					  	<ul class="pagination pagination-lg" style="margin-bottom: -12px;">								     <?php					     
					     	if(!isset($_GET['page'])){
								$_GET['page'] = 1;
							}					     
						  	for($i=1;$i<=$pages;$i++): ?>
					  	
							  <li class="<?php if($_GET['page']==$i):echo "active"; endif; ?>">
							  	<a href="<?php echo $_SERVER['PHP_SELF'].$listUrl.$i; ?>">
							  		<?php echo $i; ?>
							  			
							  	</a>
							  </li>
						<?php  endfor; ?>
					  </ul>
					    <?php	if($page!=$pages):
					     ?>
					    <li><a href="<?php echo $_SERVER['PHP_SELF'].$nextUrl; ?>">Next</a></li>
					    <?php endif; ?>
					  </ul>
					</nav>
					
			</div>
		
		</section>
		<?php include("footer.php"); ?>