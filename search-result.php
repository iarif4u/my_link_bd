<?php include("header.php"); ?>
<?php
		$subcate = (int)$_GET['subcate'];
		$district = (int)$_GET['district'];
		if($subcate==0||$district==0){ ?>
			<script type="text/javascript">
				location.href = "index.php";
			</script>
	<?php	
		}elseif(count($_GET)>5){ ?>
			<script type="text/javascript">
				location.href = "index.php";				
			</script>
	<?php	}
	
?>	
		<section id="body_area">
			<div class="container">
				<div class="row">
					<div class="col-md-3">					
						<?php include("left-sidebar.php"); ?>					
					</div>					
					<div class="col-md-9">
						<div class="product_shop_item"> 	
							<?php
								$perpage = 9;
								$total	= $data->getCountData('shopkeeper',$subcate,$district);
								$page=(isset($_GET['page'])&& $_GET['page']>0) ? (int)$_GET['page'] : 1;
								$prolimit = ($page > 1) ? ($page * $perpage)-$perpage : 0;
								$getinfo =$data->getDatabyLimit("shopkeeper",$subcate,$district,$prolimit,$perpage);	
								$prev = $page-1;
								$next = $page+1;
								$pages = ceil($total/$perpage);
								$prevUrl =	"?subcate=$subcate&district=$district&page=".$prev;
								$nextUrl = "?subcate=$subcate&district=$district&page=".$next;
								$listUrl = "?subcate=$subcate&district=$district&page=";
								
								if($getinfo){									
									foreach($getinfo as $shoplist):							
								?>			
								<div class="col-md-4"> 
									<div class="single_product_shops"> 
										
											<img src="site_img/header_image/<?php echo $shoplist['header_img']; ?>" class="img-responsive" alt="" />
											<a href="single-product.php?proid=<?php echo $shoplist['id']; ?>"><h4><?php echo $shoplist['shop_name']; ?></h4></a>
												<!--<h5><?php //echo $shoplist['shop_description']; ?></h5>-->
												<h6><b>মোবাইল </b> : <?php echo $shoplist['phone_number']; ?></h6>
												<!--<p><b>অবস্থান</b> : <?php //echo $shoplist['address']; ?></p>-->
											<a href="single-product.php?proid=<?php echo $shoplist['id']; ?>"><button type="button" class="btn btn-primary btn-block">আরো পড়ুন ..</button></a>		
										
									</div>							
								</div>
							<?php 
								endforeach; 
								}else{ ?>
									<script type="text/javascript">
										location.href = "index.php";
									</script>
							<?php	}
							?>
								
					</div>
					</div>	
				</div>
					<nav>
					  <ul class="pager">	
					   <?php if($page!=1){ ?>
					    <li><a href="<?php echo $_SERVER['PHP_SELF'].$prevUrl; ?>">Previous</a></li>
					    
					    <?php } ?>
					  
					  	<ul class="pagination pagination-lg" style="margin-bottom: -12px;">								     <?php					     
					     	if(!isset($_GET['page'])){
								$_GET['page'] = 1;
							}					     
						  	for($i=1;$i<=$pages;$i++): ?>
					  	
							  <li class="<?php if($_GET['page']==$i):echo "active"; endif; ?>">
							  	<a href="<?php echo $_SERVER['PHP_SELF'].$listUrl.$i; ?>">
							  		<?php echo $i; ?>
							  			
							  	</a>
							  </li>
						<?php  endfor; ?>
					  </ul>
					    <?php	if($page!=$pages):
					     ?>
					    <li><a href="<?php echo $_SERVER['PHP_SELF'].$nextUrl; ?>">Next</a></li>
					    <?php endif; ?>
					  </ul>
					</nav>
			</div>
		
		</section>
		<?php include("footer.php"); ?>