$(function () {
  $.scrollUp({
    scrollName: 'scrollUp', // Element ID
    topDistance: '300', // Distance from top before showing element (px)
    topSpeed: 300, // Speed back to top (ms)
    animation: 'slide', // Fade, slide, none
    animationInSpeed: 200, // Animation in speed (ms)
    animationOutSpeed: 200, // Animation out speed (ms)
    scrollText: '<i class="fa fa-chevron-up"></i>', // Text for element
    activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
  });
});



//slider code here
$(document).ready(function(){
      var owl = $("#main_slider");
	  
      owl.owlCarousel({
		items:1,
		loop:true,
		nav: false,
		autoplay:true,
		autoplayTimeout:5000,
		autoplayHoverPause:true	    
	});
     

	// Go to the next item
	$('.prev').click(function() {
		owl.trigger('next.owl.carousel');
	})
	// Go to the previous item
	$('.next').click(function() {
		owl.trigger('prev.owl.carousel');
	})
	 
     
});
	
	
//product slider code here
$(document).ready(function(){
      var owl = $("#product_slider");
	  
      owl.owlCarousel({
		items:4,
		loop:true,
		nav: false,
		margin:20,
		autoplay:true,
		autoplayTimeout:3000,
		autoplayHoverPause:true	    
	});
     

	// Go to the next item
	$('.sprev').click(function() {
		owl.trigger('next.owl.carousel');
	})
	// Go to the previous item
	$('.snext').click(function() {
		owl.trigger('prev.owl.carousel');
	})
	 
     
    });	
	
// start sticky_menu





// end sticky_menu