<?php include("header.php"); ?>
		<section id="body_area">
			<div class="container">
				<div class="row">
					<div class="col-md-3">					
						<?php include("left-sidebar.php"); ?>
					</div>
					<?php
						$number   = $_REQUEST['proid'];			
						$pro_id = validNumber($number);
							if($pro_id){
								$sopid = $pro_id;
							}else{ ?>
								<script type="text/javascript">
									location.href = "index.php";
								</script>
					<?php } ?>
					<?php  
						$getData = $data->getProductData($sopid);
					?>
					<div class="col-md-9 badcome_area">
					<?php
						$cateId = $getData['category_id'];
						$SubcateId = $getData['sub_category_id'];						
						$catearray =	$data->getDataID("add_category","id = ",$cateId);
						$subcatearray = $data->getSubcateID($SubcateId);
					?>
					
						<p>
							<a href="subcatagore-page.php?cate=<?php echo $cateId; ?>"><?php echo $catearray['category_name']; ?></a>
							>>
							<a href="shop.php?sub_cate=<?php echo $SubcateId; ?>"><?php echo $subcatearray['sub_category_name']; ?></a>
							>> 
							<?php echo $getData['shop_name']; ?> 
						</p>
					</div>
					<div class="col-md-9">
						<div class="single_product_area"> 
							
							<div class="col-md-12">
								<div class="hear_single_product">
									<h2><?php echo $getData['shop_name']; ?></h2>
									<p><?php echo $getData['address']; ?></p>
								</div>
							</div>
							<div class="col-md-12">
								<div class="product-slider-text"> 
									<div class="col-md-8">
										<div class="product-slider-img">					
											<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
											  <!-- Wrapper for slides -->
											  <div class="carousel-inner" role="listbox">
											  <?php 
											  	$img = $data->getImgbyid($sopid); 
											  	$counter = 0;
											  	if($img):
											  	foreach($img as $imglist):
											  ?>
										<div class="item <?php if($counter==0) echo "active"; ?>">
		                                <img src="site_img/item_image/<?php echo $imglist['img_name']; ?>" alt="" class="img-responsive">
												  <div class="carousel-caption">
													<?php echo $imglist['image_description']; ?>
												  </div>
												</div>		
												<?php 
													++$counter;
													endforeach; 
													else:
													echo("<center><h3>Sorry, image not found</h3></center>");	
													endif;							
												?>								
											  </div>

											  <!-- Controls -->
											  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
												<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
												<span class="sr-only">Previous</span>
											  </a>
											  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
												<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
												<span class="sr-only">Next</span>
											  </a>
											</div>
					
										</div>
									</div>
									
									<div class="col-md-4">
										
										<div class="quick-info"> 
											<h4>আমাদের তথ্য</h4>
											
											<p><i class="fa fa-user"></i>
												<?php echo $getData['owner']; ?>
											</p>
											<p><i class="fa fa-phone-square"></i>
												<?php echo $getData['phone_number']; ?>
											</p>
											<p><i class="fa fa-envelope"></i>
											<a href="mailto:<?php echo $getData['email']; ?>"><?php echo $getData['email']; ?></a>
											<p><i class="fa fa-internet-explorer"></i>
											<a target="_blank" href="url.php?url=<?php echo $getData['web_address']; ?>"><?php echo $getData['web_address']; ?></a></p>
										

											<h4>আমাদের সামাজিক মিডিয়া</h4>
											
											<ul class="social-add-area social-icons-colored">
				                            <li class="facebook"><a target="_blank" href="url.php?url=<?php echo $getData['facebook']; ?>"><i class="fa fa-facebook"></i></a></li>
												<li class="twitter"><a target="_blank" href="url.php?url=<?php echo $getData['twiter']; ?>"><i class="fa fa-twitter"></i></a></li>
												<li class="google-plus"><a target="_blank" href="url.php?url=<?php echo $getData['google_plus']; ?>"><i class="fa fa-google-plus"></i></a></li>  
											</ul>
											
										</div>
									
									</div>
								
								</div>
							</div>
							
							<div class="col-md-12">
								<div class="single_body_content"> 
									<h2> <span>বিবরণ </span> </h2>
									<p class="single_content">
										<?php echo $getData['shop_description']; ?>
									</p>
								</div>
							</div>
							<div class="col-md-12">
								<div class="single_add_product"> 
									<h2><span>আমাদের সেবাসমূহ</span> </h2>
									<?php 
										$other_imge = $data->getImgbyid($sopid); 
										foreach($other_imge as $other_img):
									?>
									<div class="col-md-4"> 
										<div class="one_product hvr-float-shadow">
											<a class="example-image-link img-responsive img-thumbnail" href="site_img/item_image/<?php echo $other_img['img_name']; ?>" data-lightbox="example-set" data-title="<?php echo $other_img['image_description']; ?>"><img class="example-image" src="site_img/item_image/<?php echo $other_img['img_name']; ?>" alt=""/></a>
										</div>
									</div>
									<?php endforeach; ?>
								</div>
							</div>
						
							<div class="col-md-12"> 
								<div class="col-md-6">
								
									<div class="single_add_con_addres"> 
										<h4><span> আমাদের সাথে যোগাযোগ করুন  </span></h4>		
											<p><i class="fa fa-user"></i><?php echo $getData['owner']; ?> </p>
											<p><i class="fa fa-phone-square"></i><?php echo $getData['phone_number']; ?></p>
											<p><i class="fa fa-envelope"></i><?php echo $getData['email']; ?></p></p>
											<p><i class="fa fa-internet-explorer"></i>
											<?php
												$link =	$getData['web_address'];
												$a = '<a href=url.php?url=';
												$b = " target='_blank'>";
												$link_insert = $a.$link.$b.$link.'</a>';
												echo $link_insert;
											?>
											</p>
											<p><i class="fa fa-map"></i><?php echo $getData['address']; ?></p>
											<p id="clidkmess"><i class="fa fa-comments"></i>click Hear</p>
											
											
									</div>
								</div>
								<div class="col-md-6">
									<div class="offer-area"> 
											<h4><span> আমাদের নতুন অফার </span> </h4>
											<div class="offer-images"> 
													<p><?php echo $getData['offer_imformation']; ?></p>
											</div>
									</div>
								</div>
							</div>

							<?php /*
	if(isset($_REQUEST['send'])){
		if(isset($_REQUEST['g-recaptcha-response'])&&$_REQUEST['g-recaptcha-response']){
			$secret 	= "6LesTBkTAAAAAIIGJfXhiElxTQ1-QZwiDy-dvAwi";
			$ip			= getUserIP();
			$captcha	= $_REQUEST['g-recaptcha-response'];
			$curl = curl_init();
			curl_setopt_array($curl, [
						CURLOPT_RETURNTRANSFER => 1,
						CURLOPT_URL	=> 'https://www.google.com/recaptcha/api/siteverify',
						CURLOPT_POST => 1,
						CURLOPT_POSTFIELDS => [
							'secret' => '6LesTBkTAAAAAIIGJfXhiElxTQ1-QZwiDy-dvAwi',
							'response' => $_REQUEST['g-recaptcha-response']
						]
			]);
			$response = json_decode(curl_exec($curl));
			$result =  $response->success;			
			if($result){
				$name = $_REQUEST['name'];
				$email = $_REQUEST['email'];
				$subject = $_REQUEST['subject'];
				$description = $_REQUEST['description'];
				if(checkString($name)&&checkString($subject)&&checkString($description)&&checkEmail($email)){
					$name       = @trim(stripslashes($_REQUEST['name'])); 
					$from       = @trim(stripslashes($_REQUEST['email'])); 
					$subject    = @trim(stripslashes($_REQUEST['subject'])); 
					$message    = @trim(stripslashes($_REQUEST['description'])); 
					$to    		= $getData['email'];	//replace with your email	
					$headers = 'From: webmaster@example.com' . "\r\n" .
								'Reply-To: webmaster@example.com' . "\r\n" .
								'X-Mailer: PHP/' . phpversion();
					  if (!mail($to, $subject, $message, $headers)) { 
						print_r(error_get_last());
					  }
					  else { ?>
						<center><h4 style="color: green; font-weight: bold;">Successfully Send</h4></center>
					  <?php
					  }
				}else{ ?>
					<center><h4 style="color: red; font-weight: bold;">Something Wrong</h4></center>
		<?php		}				
				
			}else{ ?>
				<center><h4 style="color: red; font-weight: bold;">You enter a wrong capture</h4></center>
		<?php	}			
		}else{ ?>
			<center><h4 style="color: red; font-weight: bold;">Capture doesn't match</h4></center>
	<?php	}			
	}
	function getUserIP(){
	    $client  = @$_SERVER['HTTP_CLIENT_IP'];
	    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
	    $remote  = $_SERVER['REMOTE_ADDR'];

	    if(filter_var($client, FILTER_VALIDATE_IP)){
	       $ip = $client;
	    }
	    elseif(filter_var($forward, FILTER_VALIDATE_IP)){
	        $ip = $forward;
	    }
	    else{
	        $ip = $remote;
	    }
	    return $ip;
	}	
	*/
?>
							<div class="col-md-12"> 
								<div id="addmess"class="sing_Mess"> 
									
				<form method="GET" action="<?php echo $_SERVER['PHP_SELF'] ?>" class="form-horizontal">
										<div class="form-group">

											<div class="col-sm-6">
											<input type="hidden" value="<?php echo $sopid; ?>" name="proid" />
												<input type="text" name="name" class="form-control" id="inputEmail3" placeholder="আপনার নাম লিখুন">
											</div>
											<div class="col-sm-6">
												<input type="email" name="email" class="form-control" id="inputPassword3" placeholder="আপনার ইমেইল লিখুন">
											</div>

										</div>
										<div class="form-group">

											<div class="col-sm-12">
					<input type="text" name="subject" class="form-control" id="inputPassword3" placeholder="আপনার বিষয় লিখুন">					
											</div>

										</div>
										<div class="form-group">

											<div class="col-sm-12">
												<textarea name="description" class="form-control" placeholder="আপনার বার্তা লিখুন...." rows="3"></textarea>
											</div>
										
										</div>
										
										<div class="form-group">

											<div class="col-sm-6">
												<div class="g-recaptcha" data-sitekey="6LesTBkTAAAAAECE2FBiXT3Yiug4m8UpxUaHifM-"></div>
											</div>
											<div class="col-sm-6">
												
											</div>
										
										</div>
										
										<div class="form-group">

											<div class="col-sm-6">
												<button name="send" type="submit" class="btn btn-primary">Send You Messag</button>
											</div>
											<div class="col-sm-6">
												
											</div>
										
										</div>


									</form>
																			
									
								</div>
							</div>
							
							
							<div class="col-md-12"> 
								<div class="sin_pro_footer"> 
									<div class="col-md-6 left_footer_area">
										<p> © 2016 MyLinkBd | All Rights Reserved </p>
									</div>
									<div class="col-md-6 right_footer_area">
										<p> Developer By: <a href="http://differentcoder.com/"> Different Coder </a>  </p>
									</div>
								</div>
							</div>
							
						</div>
					</div>	

				</div>
					
			</div>
		
		</section>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script>
$(document).ready(function(){
    $("#clidkmess").click(function(){
        $("#addmess").toggle(1000);
    });
});
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
	
				
<?php include("footer.php"); ?>