<?php
	if(basename($_SERVER['PHP_SELF']) == basename(__FILE__)){
		die("<center><h3>Forbidden...</h3><h4>Sorry, Direct access not allowed<h4></center>");
	}
	class MyDBConnection{
		private $connect;
				
		function connection($host,$dbname,$user,$password){
			try{
				$this->connect = new PDO("mysql:host=$host;dbname=$dbname",$user,$password);
				$this->connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->connect->query('SET NAMES utf8');
				return $this->connect;
			} 
			catch (PDOException $e) {
				die('<center><h3>Connection failed: ' . $e->getMessage()."</center></h3>");
			}
			
		}
	}
?>