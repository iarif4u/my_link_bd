<?php include("header.php"); ?>
		<section id="body_area">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<?php include("left-sidebar.php"); ?>
					</div>
					<div class="col-md-9">
						<div class="top_Categories"> 
	<?php
		$getkey = array_keys($_GET);
		if($getkey[0]=="cate"){			
			$number   = (int)$_GET['cate'];			
			$cate_id = validNumber($number);
			$perpage = 20;
			$total	= $data->getCountDatabyCate($cate_id);
			$page=(isset($_GET['page'])&& $_GET['page']>0) ? (int)$_GET['page'] : 1;
			$prolimit = ($page > 1) ? ($page * $perpage)-$perpage : 0;
			$prev = $page-1;
			$next = $page+1;
			$pages = ceil($total/$perpage);
			$prevUrl =	"?cate=$cate_id&page=".$prev;
			$nextUrl =	"?cate=$cate_id&page=".$next;
			$listUrl = "?cate=$cate_id&page=";
			if($cate_id){
				$sub_cate = $data->getLimtData('sub_category',"category_id=",$cate_id,$prolimit,$perpage);		
		if($sub_cate != false){
			foreach($sub_cate as $sub_row){ ?>
			
								<div class="cat_img_name"> 
									
								
										<a href="shop.php?sub_cate=<?php echo $sub_row['id']; ?>">
					<img src="site_img/sub_cat_image/<?php echo $sub_row['image']; ?>" class="img-responsive" alt="" />
											<span><?php echo $sub_row['sub_category_name']; ?></span>
										</a>
									
									
								</div>
		<?php 
			}
		}else{ ?>
			<h1>Sorry, Data is not available now...</h1>
	<?php	} 
		?>
						</div>
					</div>
				</div>
				<nav>
					  <ul class="pager">	
					   <?php if($page!=1){ ?>
					    <li><a href="<?php echo $_SERVER['PHP_SELF'].$prevUrl; ?>">Previous</a></li>
					    
					    <?php } ?>
					  
					  	<ul class="pagination pagination-lg" style="margin-bottom: -12px;">								     <?php					     
					     	if(!isset($_GET['page'])){
								$_GET['page'] = 1;
							}					     
						  	for($i=1;$i<=$pages;$i++): ?>
					  	
							  <li class="<?php if($_GET['page']==$i):echo "active"; endif; ?>">
							  	<a href="<?php echo $_SERVER['PHP_SELF'].$listUrl.$i; ?>">
							  		<?php echo $i; ?>
							  			
							  	</a>
							  </li>
						<?php  endfor; ?>
					  </ul>
					    <?php	if($page!=$pages):
					     ?>
					    <li><a href="<?php echo $_SERVER['PHP_SELF'].$nextUrl; ?>">Next</a></li>
					    <?php endif; ?>
					  </ul>
					</nav>
					
			</div>
		
		</section>
	<?php 
	
			}else{ ?>
				<script type="text/javascript">
					location.href = "index.php";
				</script>
	<?php	}
		}else{ ?>
				<script type="text/javascript">
					location.href = "index.php";
				</script>
		<?php	}
	
	include("footer.php");
	 ?>
