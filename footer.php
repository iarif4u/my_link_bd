		
		<section id="footer_area"> 
			
			<!--	<div class="footer">
					<div class="container">	

						<div class="col-md-3">
							<div class="footer-item">
							<h2>About us</h2>
							<p>
							Glatt Limousinenservice GmbH is the premier limousine service provider
							in Zurich. We are specialized to the needs of business travelers.
							We are able to bring at any time our passengers on time, 
							safely and comfortably to your destination.
							</p>
							</div>
						</div>	

						<div class="col-md-3">
							<div class="footer-item">
							<h2>WE SERVE</h2>				
							<div class="footer-menu">
								<ul>
									<li><a href="#"> About Us <i class="fa fa-angle-right"></i></a></li>
									<li><a href="#"> Services  <i class="fa fa-angle-right"></i></a></li>
									<li><a href="#"> Prices <i class="fa fa-angle-right"></i></a></li>
									<li><a href="#"> Online Request <i class="fa fa-angle-right"></i></a></li>
								</ul>
							</div>
							</div>
						</div>	

						<div class="col-md-3">
							<div class="footer-item">
							<h2>Usefull Links</h2>
							
							<div class="footer-menu">
								<ul>
									<li><a href="#">  Business Service <i class="fa fa-angle-right"></i></a></li>
									<li><a href="#">  Airport transfers <i class="fa fa-angle-right"></i></a></li>
									<li><a href="#">  Excursions <i class="fa fa-angle-right"></i></a></li>
									<li><a href="#">  Events <i class="fa fa-angle-right"></i></a></li>
									<li><a href="#">  World Economic <i class="fa fa-angle-right"></i></a></li>
									<li><a href="#">  ART Basel <i class="fa fa-angle-right"></i></a></li>
								</ul>
							</div>
							
							
							</div>
						</div>	

						<div class="col-md-3">
							<div class="footer-item">
							<h2>Types of Business</h2>				
							<div class="footer-menu">
								<ul>
									<li><a href="#"> About Us <i class="fa fa-angle-right"></i></a></li>
									<li><a href="#"> Services  <i class="fa fa-angle-right"></i></a></li>
									<li><a href="#"> Prices <i class="fa fa-angle-right"></i></a></li>
									<li><a href="#"> Online Request <i class="fa fa-angle-right"></i></a></li>
								</ul>
							</div>
							</div>
						</div>

					</div>			
				</div>	-->

				<div class="copyright">
					<div class="container">	
						<div class="col-md-4">
							<div class="f_copyright"> 
								<p>Copyright &copy; 2016 MyLinikBd.All Rights Reserved</p>
							</div>
						</div>	
						
						<div class="col-md-4">
							<div class="f_social">
								
								<ul class="social-icons social-icons-colored">
									<li class="facebook"><a target="_blank" href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
									<li class="twitter"><a target="_blank" href="http://twitter.com"><i class="fa fa-twitter"></i></a></li>
									<li class="google-plus"><a target="_blank" href="http://www.facebook.com"><i class="fa fa-google-plus"></i></a></li>
									<li class="youtube"><a target="_blank" href="http://www.facebook.com"><i class="fa fa-youtube"></i></a></li>
										
								</ul>			
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="f_development"> 
								<p>Design & Development By  <a href="http://differentcoder.com/"><img src="images/developer_logo.png" alt="Defferent Coder" /></a></p>
							</div>
						</div>
						

					</div>			
				</div>	
			
		</section>
				
      
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.3.min.js"><\/script>')</script>
        <script src="js/jquery.scrollUp.js"></script>
        <script src="js/jquery-scrolltofixed-min.js"></script>
        <script src="js/bootstrap.min.js"></script>
     
        <script src="js/lightbox-plus-jquery.min.js"></script>
        
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script type="text/javascript">
       			function ajaxLoad(){
					return '<img src="my_link_admin/ajax/loading.gif" alt="" />';
				}
        	function selectDivision(value){      
        		$("#setDistrict").html(ajaxLoad());  		
				$.get('my_link_admin/ajax/ajax.inc.php',
				{action : 1 , data : value},
					function(result){
						$("#setDistrict").html(result);
					}
				)
			}
			function selectCategory(value){
				$("#setSubcate").html(ajaxLoad());
				$.get('my_link_admin/ajax/ajax.inc.php',
				{action : 2 , data : value},
					function(result){
						$("#setSubcate").html(result);
					}
				)
			}
        </script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script> -->
    </body>
</html>
