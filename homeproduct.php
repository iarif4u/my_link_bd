<?php include("header.php"); ?>
			<section id="body_area">
				<div class="container">
					<div class="row">
						
						<div class="col-md-12">
							<div class="AddProduct">
								<h3>অনলাইন বাজার</h3>
								<div class="">
								  <!-- Nav tabs -->
								  <ul class="nav nav-tabs" role="tablist">
									<li role="presentation" class="active"><a href="#chittagong" aria-controls="chittagong" role="tab" data-toggle="tab">চট্টগ্রাম</a></li>
									<li role="presentation"><a href="#dhaka" aria-controls="dhaka" role="tab" data-toggle="tab">ঢাকা</a></li>
									<li role="presentation"><a href="#" aria-controls="Rajshahi" role="tab" data-toggle="tab">রাজশাহী </a></li>
									<li role="presentation"><a href="#" aria-controls="Khulna" role="tab" data-toggle="tab">খুলনা </a></li>
									<li role="presentation"><a href="#" aria-controls="Barisal" role="tab" data-toggle="tab">বরিশাল </a></li>
									<li role="presentation"><a href="#" aria-controls="Sylhet" role="tab" data-toggle="tab">সিলেট </a></li>
									<li role="presentation"><a href="#" aria-controls="Rangpur" role="tab" data-toggle="tab">রংপুর </a></li>
									<li role="presentation"><a href="#" aria-controls="Mymensingh" role="tab" data-toggle="tab">ময়মনসিংহ </a></li>
								  </ul>

								  <!-- Tab panes -->
								  <div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="chittagong">
									`<div class="header_area"> 
										<h4>চট্টগ্রামের অনলাইন বাজার</h4>
									</div>
									<div class="content">
										<div class="row">
										<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">				
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/d_Photographer.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='https://www.facebook.com/photosnapbd/?fref=ts'" target="_blank" data-toggle="modal" data-target="#modal10" >PhotoGrapher
														</button>
														<!--<p class="icon-links">
															<a href="https://www.facebook.com/interroyal.lighting/?fref=ts"><span class="fa fa-twitter"></span></a>
															<a href="https://www.facebook.com/interroyal.lighting/?fref=ts"><span class="fa fa-facebook"></span></a>
															<a href="https://www.facebook.com/interroyal.lighting/?fref=ts"><span class="fa fa-instagram"></span></a>
														</p>-->
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
											<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">			
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/c1.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='https://www.facebook.com/interroyal.lighting/?fref=ts'" target="_blank" data-toggle="modal" data-target="#modal10"> Lighting 
														</button>
													
														
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
										
											<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">			
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/d_Watch.jpg" alt="">
													<div class="overlay">
														<button onclick="location.href='http://www.nokshactg.com/'" class="info" target="_blank" data-toggle="modal" data-target="#modal10"> Watch
														</button>
														
														
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
										
											<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">			
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/c3.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='http://okbazaar.com.bd/'"  target="_blank" data-toggle="modal" data-target="#modal10"> T-shirt 
														</button>
														
														
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
											
										</div> 
										<!-- End Row Area -->
									</div>
									<!-- End Content Area -->
									</div>
									<!-- End Chittagong Style Site -->
									
									<div role="tabpanel" class="tab-pane" id="dhaka">
										
									<div class="header_area"> 
										<h4>ঢাকার অনলাইন বাজার...</h4>
									</div>
									<div class="content">
										<div class="row">
										
										<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">				
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/d_Kamiz.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='https://www.facebook.com/FashionTrendzz1/?fref=ts'" target="_blank" data-toggle="modal" data-target="#modal10"> Kamiz/Three Piece </button>
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
											<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">				
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/d_Saree.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='https://www.facebook.com/candlesbd24/?fref=ts'" target="_blank" data-toggle="modal" data-target="#modal10" > Saree </button>
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
											<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">				
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/d_T-Shirt1.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='https://www.facebook.com/mhtraders15/?fref=ts'" target="_blank" data-toggle="modal" data-target="#modal10" >T-Shirt </button>
														
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
											<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">				
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/d_Watch.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='https://www.facebook.com/jahandeal/?fref=ts'" target="_blank" data-toggle="modal" data-target="#modal10" >Watch</button>
														
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
											<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">				
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/d_Shoe.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='https://www.facebook.com/sustvoice/?fref=ts'" target="_blank" data-toggle="modal" data-target="#modal10" >Shoe/Sandle</button>
														
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
											<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">				
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/d_Electronics.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='https://www.facebook.com/DesiWestern/?fref=ts'" target="_blank" data-toggle="modal" data-target="#modal10" >Electronics</button>
														
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
											<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">				
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/d_BedSheet.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='https://www.facebook.com/DREAM-HOME-Collection-780074375418459/?fref=ts'" target="_blank" data-toggle="modal" data-target="#modal10" > Bed Sheet </button>
														
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
											<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">				
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/d_Ring.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='https://www.facebook.com/dhakameladotcom?fref=ts&ref=br_tf&qsefr=1'" target="_blank" data-toggle="modal" data-target="#modal10" > Ring  </button>
														
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
											<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">				
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/d_Furniture.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='https://www.facebook.com/customerbackup/?fref=ts'" target="_blank" data-toggle="modal" data-target="#modal10" > Furniture </button>
														
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
										
											<!-- Add Section Start -->
										<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 addsection thumbnail ">				
											<div class="hover ehover10">
												<img class="img-responsive" src="images/addimg/d_Photographer.jpg" alt="">
													<div class="overlay">
														<button class="info" onclick="location.href='https://www.facebook.com/Weddinframes/?fref=ts'" target="_blank" data-toggle="modal" data-target="#modal10" > PhotoGrapher </button>
														
													</div>				
											</div>
										</div>
										<!-- Add Section End -->
										
											
										</div> 
										<!-- End Row Area -->
									</div>
									<!-- End Content Area -->
										
									
									</div>
									<!-- End Dhaka Style Site -->
									<div role="tabpanel" class="tab-pane" id="Rajshahi">রাজশাহী অনলাইন বাজার...</div>
									<div role="tabpanel" class="tab-pane" id="Khulna">খুলনা অনলাইন বাজার...</div>
									<div role="tabpanel" class="tab-pane" id="Barisal">বরিশাল অনলাইন বাজার...</div>
									<div role="tabpanel" class="tab-pane" id="Sylhet">সিলেট অনলাইন বাজার...</div>
									<div role="tabpanel" class="tab-pane" id="Rangpur">রংপুর অনলাইন বাজার...</div>
									<div role="tabpanel" class="tab-pane" id="Mymensingh">ময়মনসিংহ অনলাইন বাজার...</div>
								  </div>

								</div>
							</div>
						</div>
			
						<hr>
						
					</div>	
				</div>
		
			</section>
	
<?php include("footer.php"); ?>