<?php include("header.php"); ?>
<section id="body_area">
	<div class="container">
		<div class="row">
			<div class="col-md-9 clearfix">
				<div class="home_information">
					<div class="welcome_area">
						<div class=""> 
							<img alt="" src="images/mylinkbd_ban.jpg" class="img-responsive">
						</div>
						<div class="home_text">
								
							
							<h4> যে কোনো তথ্য জানার জন্য কিংবা অভিযোগের জন্য অনুগ্রহ পূর্বক ফোন করুন কিংবা ইমেইল করুন . </h4>
							<p><b> ফোন :+088 01825 303 917, +088 01754 488 963 </b></p>
							<p><b> ইমেইল : info@mylinkbd.com , mylinkbd2016@gmail.com </b></p>
						</div>				
								

                    </div>
				</div>
			</div>
			<?php include('quick_search.php'); ?>
		</div>
	</div>
</section>
<section id="product-cetegore">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="home_catagore">
					<div class="home_cat_title">
						<h2>
							শীর্ষ বিভাগ থেকে পণ্য  নির্বাচন করুন

						</h2>
					</div>

					<div class="top_Categories">
						<?php
						foreach($cate as $category): ?>
						<div class="cat_img_name">
							<a href="subcatagore-page.php?cate=<?php echo $category['id']; ?>">
								<img src="site_img/cat_image/<?php echo $category['image']; ?>" alt="" class="img-rounded img-responsive" />
								<h5>
									<?php echo $category['category_name']; ?>
								</h5>
							</a>
						</div>
						<?php endforeach; ?>

					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<?php include("footer.php"); ?>