<!doctype html>
<html>
    <head>
		<meta charset="utf-8" />		
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>MYLink Collection</title>
        <meta name="description" content="mylinkbd.com">
        <meta name="viewport" content="width=device-width, initial-scale=1">		
		<link rel="shortcut icon" href="favicon.ico" >
        
        <!-- Place favicon.ico in the root directory -->

        
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/lightbox.min.css">

     
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<?php 
	error_reporting(0);
	require_once('my_link_admin/controller/operation.inc.php'); 
	include_once 'my_link_admin/controller/valid.inc.function.php';	
?>

        
        <section id="header">

			<div class="container">
				<div class="row">
					
					<div class="top_header_bar clearfix">

						<div class="col-md-3 clearfix">
							<div class="logo"> 
								<a href="index.php"><img src="images/mylinkbdlogo.png" alt="" /></a>
							</div>
						</div>
						<div class="col-md-5 clearfix">
							<div class="top_header_menu"> 
								<a href="homeproduct.php" class="btn-success butadd">ঘরে বসে যে কোনো পণ্য গ্রহণ করতে এখানে ক্লিক করুন</a>
							</div>
							
							<!--<div class="top_header_menu">
								<ul>
									<li></li>
								</ul>
							</div>	-->						
						</div>
						<div class="col-md-4 clearfix"> 
							<div class="all_product_button">
							
								<!-- Large modal -->
									<a href="index.php" class="allproductbutton">হোম পেজ</a>
									<a href="#" class="allproductbutton" data-toggle="modal" data-target=".bs-example-modal-lg">সব বিভাগ</a>
									<a href="about.php" class="allproductbutton" >আমাদের সম্পরকে </a> 
									<a href="help.php" class="allproductbutton" >সাহায্য </a>
								<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
									<div class="modal-dialog modal-lg">
									  
										<div class="modal-content allproductbutton_bag">
											<div class="container-fluid">
												<div class="row">
													<div class="col-md-12"> 
														<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													</div>
												</div>
											</div>
											 
											 <div class="container-fluid">
												<div class="row">
												<?php 													
													$cate = $data->ListCategory();
													foreach($cate as $catelist) :
												 ?>
												 <div class="col-md-3 allproduclist">
													<ul>
														<li>
															<a href="subcatagore-page.php?cate=<?php echo $catelist['id']; ?>"><?php echo $catelist['category_name']; ?></a>
														</li>
														
													</ul>
												  </div>
												  <?php endforeach; ?>
												</div>
											</div>
										</div>
									 </div>
								</div>
							</div>
						</div>
						
					</div>
					
				
				</div> <!--row  end -->
			</div> <!-- container end -->
		
		</section>
	